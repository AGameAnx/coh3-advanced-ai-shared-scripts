local max = math.max

local TeamIndex = BTBUtils.TeamIndex
local IsSquadVehicle = BTBUtils.IsSquadVehicle
local sformat = string.format

local eps = 0.001

---@class AAI_LockStatus : integer
---@class AAI_LockStatusTable : AAI_LockStatus[]
AAI_LockStatus = {
	__skipsave = true,

	---@type AAI_LockStatus
	None = 0,
	---@type AAI_LockStatus
	UnitControl = 1,
	---@type AAI_LockStatus
	VehicleMovement = 2,
	---@type AAI_LockStatus
	CUC = 3,
	---@type AAI_LockStatus
	UnitAction = 4,
	---@type AAI_LockStatus
	Help = 5,
	---@type AAI_LockStatus
	Retreat = 6,
	---@type AAI_LockStatus
	Pullback = 7,
	---@type AAI_LockStatus
	AvoidDanger = 8,
	---@type AAI_LockStatus
	EngineerControl = 9,
	---@type AAI_LockStatus
	OOCCustomPositioning = 10,
}
local aai_lockStatus = AAI_LockStatus

---@param lockStatus AAI_LockStatus
---@return string
function AAI_LockStatusToString(lockStatus)
	for i,v in pairs(aai_lockStatus) do
		if v == lockStatus then return i end
	end
	return "None"
end

---@class AAI_C_PullbackCounterInfo : class
---@field count integer
---@field decreaseTimer number
---@field increaseTimer number
AAI_C_PullbackCounterInfo = {
	__skipsave = true,
}

---@return AAI_C_PullbackCounterInfo
function AAI_C_PullbackCounterInfo:new()
	local o = {
		count = 0,
		decreaseTimer = 0.0,
		increaseTimer = 0.0,
	}
	o.__index = self
	setmetatable(o, o)
	return o
end

function AAI_C_PullbackCounterInfo:TryIncrease()
	local gameTime = World_GetGameTime()
	if self.increaseTimer < gameTime then
		self.count = self.count + 1
		self.decreaseTimer = gameTime + AAI_Personality.Pullback_RecentPullbackCounterDecreasePeriod
		self.increaseTimer = gameTime + AAI_Personality.Pullback_RecentPullbackCounterIncreasePeriod
	end
end

function AAI_C_PullbackCounterInfo:TryDecrease()
	local gameTime = World_GetGameTime()
	if self.decreaseTimer < gameTime then
		self.count = max(0, self.count - 1)
		self.decreaseTimer = gameTime + AAI_Personality.Pullback_RecentPullbackCounterDecreasePeriod
		self.increaseTimer = gameTime + AAI_Personality.Pullback_RecentPullbackCounterIncreasePeriod
	end
end

function AAI_C_PullbackCounterInfo:Reset()
	self.count = 0
	self.decreaseTimer = 0.0
	self.increaseTimer = 0.0
end

---@class AAI_C_UnitControlTargetInfo : class
---@field active boolean
---@field setupPos Position
---@field headingPos Position
AAI_C_UnitControlTargetInfo = {}

---@param o AAI_C_UnitControlTargetInfo?
---@return AAI_C_UnitControlTargetInfo
function AAI_C_UnitControlTargetInfo:new(o)
	o = o or {}

	o.active = false
	o.setupPos = World_Pos(0.0, 0.0, 0.0)
	o.headingPos = World_Pos(0.0, 0.0, 0.0)

	o.__index = self
	setmetatable(o, o)
	return o
end

---@param setupPos any
---@param headingPos any
function AAI_C_UnitControlTargetInfo:Activate(setupPos, headingPos)
	self.active = true
	self.setupPos = setupPos
	self.headingPos = headingPos
end

---@class AAI_C_SquadInfoClass : class
---@field squad Squad
---@field squadID SquadID
---@field bpName BPName
---@field lockStatus AAI_LockStatus
---@field retreatType AAI_RetreatType
---@field firstSeenTime number
---@field taskingType AAI_C_MilitaryTargetType
---@field lastClosestEnemy Squad | Entity | nil
---@field lastClosestEnemyDistance number
---@field lastSafetyEval number
---@field lastRetreatDemand number
---@field task AAI_C_MilitaryPoint?
---@field taskMovePos Position?
---@field taskArrivalLockTime number
---@field taskStartingPriority integer
---@field entityToPickUp EntityID
---@field priorityQueueValue_Base number
---@field priorityQueueValue_Accumulated number
---@field hasChosenUpgrade boolean
---@field healthDelta number
---@field repairSquads SquadID[]
---@field isBeingHealed boolean
---@field lastUpdateHealth number
---@field chosenUpgrade AAI_C_PurchaseSetting | UpgradePBG | nil
---@field movePos Position
---@field assaultTime number
---@field assaultLockTimer number
---@field pullbackLockTimer number
---@field helpTime number
---@field helpCommandTimer number
---@field engineerControlLockTimer number
---@field unblobTimer number
---@field vehicleRetreatTimer number
---@field vehicleFacingTimer number
---@field vehicleEngineerRequestTime number
---@field appliedUpgrades string[]
---@field pullbackCounterInfo AAI_C_PullbackCounterInfo
---@field unitControlTargetInfo AAI_C_UnitControlTargetInfo
---@field cucSetting AAI_CombatUnitControlSetting
---@field ucSetting AAI_UnitControlSetting?
---@field repairActions AAI_UnitAction[]?
---@field oocActions AAI_UnitAction[]?
---@field captureActions AAI_UnitAction[]?
---@field guActions AAI_UnitAction[]?
---@field ecSetting table?
---@field vcSetting AAI_C_VehicleSetting
---@field vcRetreatSetting AAI_C_VehicleRetreatSetting
---@field bcSetting AAI_BarrageControlSetting?
---@field excludedFromHelp boolean
---@field activeAction AAI_UnitAction?
AAI_C_SquadInfoClass = {
	__skipsave = true,
}

function AAI_C_SquadInfoClass:__tostring()
	local squadName
	if Squad_IsValid(self.squadID) then
		squadName = BP_GetName(Squad_GetBlueprint(self.squad))
	else
		squadName = "INVALID"
	end
	return "SqInfo (name: "..squadName.." lock: "..AAI_LockStatusToString(self.lockStatus)..")"
end

---@return AAI_C_SquadInfoClass
function AAI_C_SquadInfoClass:new(squad)
	local o = {
		squad = squad,
		squadID = Squad_GetID(squad),

		lockStatus = aai_lockStatus.None,

		firstSeenTime = World_GetGameTime(),

		taskingType = AAI_C_MilitaryTarget_Default,

		lastClosestEnemy = nil,
		lastClosestEnemyDistance = 99999.0,
		lastSafetyEval = 0.0,
		lastRetreatDemand = 0.0,

		task = nil,
		taskMovePos = nil,
		taskArrivalLockTime = 0.0,
		taskStartingPriority = 0.0,

		entityToPickUp = 0,

		priorityQueueValue_Accumulated = 0.0,
		priorityQueueValue_Base = 0.0,

		hasChosenUpgrade = false,
		chosenUpgrade = nil,

		healthDelta = 0.0,
		isBeingHealed = false,
		lastUpdateHealth = Squad_GetHealthPercentage(squad, true),

		repairSquads = {},

		movePos = nil,
		assaultTime = -AAI_Personality.Assault_NoActionTime,
		assaultLockTimer = 0.0,
		pullbackLockTimer = 0.0,
		helpTime = 0.0,
		helpCommandTimer = 0.0,
		engineerControlLockTimer = 0.0,
		unblobTimer = 0.0,
		vehicleRetreatTimer = 0.0,
		vehicleFacingTimer = 0.0,
		vehicleEngineerRequestTime = 0.0,

		appliedUpgrades = {},

		unitControlTargetInfo = AAI_C_UnitControlTargetInfo:new(),
		pullbackCounterInfo = AAI_C_PullbackCounterInfo:new(),

		activeAction = nil,

		__index = self,
	}

	setmetatable(o, {__index = self})

	o.priorityQueueValue_Base = o:GetBasePriorityQueueValue()

	o:UpdateBPNameReferences()

	return o
end

---@param force? boolean
function AAI_C_SquadInfoClass:UpdateBPNameReferences(force)
	local bpName = BP_GetName(Squad_GetBlueprint(self.squad))
	if force or self.bpName ~= bpName then
		self.bpName = bpName
		self.retreatType = AAI_Personality.RetreatType[bpName] or AAI_RetreatType_Normal
		self.cucSetting = AAI_Personality.CombatUnitControl_UnitSettings[bpName] or AAI_CombatUnitControlSetting
		self.ucSetting = AAI_Personality.UnitControl_UnitSettings[bpName]
		self.repairActions = AAI_Personality.RepairControl_Actions[bpName]
		self.oocActions = AAI_Personality.OutOfCombatControl_Actions[bpName]
		self.captureActions = AAI_Personality.OutOfCombatControl_CaptureActions[bpName]
		self.guActions = AAI_Personality.GeneralUseActions[bpName]
		self.ecSetting = AAI_Personality.EngineerControl[bpName]
		self.vcSetting = AAI_Personality.VehicleControl_CustomVehicles[bpName] or AAI_C_VehicleSetting
		self.vcRetreatSetting = AAI_Personality.VehicleControl_VehicleRetreatSettings[bpName] or AAI_C_VehicleRetreatSetting
		self.bcSetting = AAI_Personality.BarrageControl[bpName]
		self.excludedFromHelp =  AAI_Personality.Help_ExcludedUnits[bpName] or false
		self.priorityQueueValue_Base = self:GetBasePriorityQueueValue()
		self.taskingType = AAI_Personality.MilitaryPoint_SquadTargetTypes[self.bpName]
		if not self.taskingType then
			if BTBUtils.IsSquadVehicle(self.squad) then
				self.taskingType = AAI_C_MilitaryTarget_Vehicle
			else
				self.taskingType = AAI_C_MilitaryTarget_Default
			end
		end
	end
end

function AAI_C_SquadInfoClass:StopEntityPickup()
	local teamIndex = TeamIndex(Squad_GetPlayerOwner(self.squad))
	if AAI_C_EntityPickUpInfo[teamIndex][self.entityToPickUp] and AAI_C_EntityPickUpInfo[teamIndex][self.entityToPickUp][1] > 0 then
		AAI_C_EntityPickUpInfo[teamIndex][self.entityToPickUp] = nil
	end
	self.entityToPickUp = 0
end

function AAI_C_SquadInfoClass:GetBasePriorityQueueValue()
	local result = AAI_Personality.PriorityQueue_BasePriority

	if self.ucSetting then
		result = result + AAI_Personality.PriorityQueue_UnitControlValue
		if not self.ucSetting.canAttackMove then
			result = result + AAI_Personality.PriorityQueue_NoAttackMove
		end
	elseif self.vcSetting and not self.vcSetting.canAttackMove then
		result = result + AAI_Personality.PriorityQueue_NoAttackMove
	end

	local squadType = AAI_Personality.MilitaryPoint_SquadTargetTypes[self.bpName]
	if not squadType then
		if IsSquadVehicle(self.squad) then
			squadType = AAI_C_MilitaryTarget_Vehicle
		else
			squadType = AAI_C_MilitaryTarget_Default
		end
	end
	result = result + (AAI_Personality.PriorityQueue_SquadTargetTypeValue[squadType] or 0.0)

	return result
end

---@return number
function AAI_C_SquadInfoClass:GetExtraPriorityQueueValue()
	local squad = self.squad

	local inCombatDuration = AAI_C.GetInCombatDuration()
	local isVehicle = IsSquadVehicle(squad)
	local healthPercent = Squad_GetHealthPercentage(squad, true)

	local result = (1.0 - healthPercent) * AAI_Personality.PriorityQueue_HealthPercent

	local isLowHPVehicleNearHQ = false
	if isVehicle and healthPercent < 0.95
			and World_DistanceSquaredPointToPoint(Squad_GetPosition(squad), AAI_C.GetVehicleHQRetreatPos(squad)) < AAI_Personality.VehicleControl_AcceptableHQMoveRadius ^ 2 then
		isLowHPVehicleNearHQ = true
		result = result + AAI_Personality.PriorityQueue_LowHealthNearHQVehicleValue
	end

	if Squad_IsUnderAttack(squad, inCombatDuration) or Squad_IsAttacking(squad, inCombatDuration) then
		result = result + AAI_Personality.PriorityQueue_InCombatValue
	elseif not isLowHPVehicleNearHQ
			and World_GetGameTime() > self.vehicleRetreatTimer
			and not Squad_IsMoving(squad)
			and not AAI_C.IsSquadCapturing(squad)
			and (isVehicle or (
				not Squad_IsConstructing(squad)
				and not (Squad_CanInstantReinforceNow(squad) or Squad_IsReinforcing(squad))
			)) then
		result = result + AAI_Personality.PriorityQueue_Idle
	elseif self.oocActions then
		result = result + AAI_Personality.PriorityQueue_HasOOCActions
	end

	if self.assaultLockTimer > eps then
		result = result + AAI_Personality.PriorityQueue_RecentlyAssaultingValue
	end

	if self.pullbackLockTimer > eps then
		result = result + AAI_Personality.PriorityQueue_RecentlyPulledBackValue
	end

	if self.lockStatus == aai_lockStatus.EngineerControl then
		result = result + AAI_Personality.PriorityQueue_EngineerControlValue
	elseif self.lockStatus == aai_lockStatus.Retreat then
		result = result + AAI_Personality.PriorityQueue_RetreatedSquadValue
	end

	if self.lastClosestEnemyDistance < AAI_Personality.PriorityQueue_EnemyProximity_Max then
		result = result + AAI_Personality.PriorityQueue_EnemyProximityValueBase
			+ AAI_Personality.PriorityQueue_EnemyProximityValueExtra * (1.0 - max(0.0, self.lastClosestEnemyDistance - AAI_Personality.PriorityQueue_EnemyProximity_Start)
				/ max(1.0, AAI_Personality.PriorityQueue_EnemyProximity_Max - AAI_Personality.PriorityQueue_EnemyProximity_Start))
	end

	return result
end

---@param newLockStatus AAI_LockStatus
---@param taskAway? boolean
function AAI_C_SquadInfoClass:Lock(newLockStatus, taskAway)
	--DSText(self.squad, self.squadID.."_lock", sformat("Locked %s => %s", AAI_LockStatusToString(self.lockStatus), AAI_LockStatusToString(newLockStatus)))
	local lockStatus = self.lockStatus
	if newLockStatus ~= aai_lockStatus.Help and lockStatus == aai_lockStatus.Help then
		self.assaultLockTimer = 0.0
	end
	if newLockStatus ~= aai_lockStatus.Pullback and lockStatus == aai_lockStatus.Pullback then
		self.pullbackLockTimer = 0.0
	end

	if self.activeAction then
		self.activeAction:Deactivate(self, false)
		self.activeAction = nil
	end

	if self.entityToPickUp > 0 then
		self:StopEntityPickup()
	end

	if (taskAway or taskAway == nil) and self.task then
		--print("Locking squad "..self.bpName.." with task "..tostring(self.task)..": "..AAI_LockStatusToString(newLockStatus))
		self.task:OnSquadTaskedAway(self)
	end

	--local squad = self.squad
	--local player = Squad_GetPlayerOwner(squad)
	--if AI_IsAIPlayer(player) then
	--	AI_LockSquad(player, squad)
	--end

	if newLockStatus ~= lockStatus then
		self:DismissRepairSquads()

		self.lockStatus = newLockStatus
	end
end

function AAI_C_SquadInfoClass:Unlock()
	--DSText(self.squad, self.squadID.."_lock", sformat("Unlcked %s", AAI_LockStatusToString(self.lockStatus)))
	local lockStatus = self.lockStatus
	if lockStatus == aai_lockStatus.Help then
		self.assaultLockTimer = 0.0
	end
	if lockStatus == aai_lockStatus.Pullback or lockStatus == aai_lockStatus.AvoidDanger then
		self.pullbackLockTimer = 0.0
	end

	self.taskArrivalLockTime = 0.0

	if self.entityToPickUp > 0 then
		self:StopEntityPickup()
	end

	if self.activeAction then
		self.activeAction:Deactivate(self, false)
		self.activeAction = nil
	end

	self.lockStatus = aai_lockStatus.None
end

function AAI_C_SquadInfoClass:DismissRepairSquads()
	local repairSquadsCopy = BTBUtils.ShallowCopy(self.repairSquads)
	for i=1,#repairSquadsCopy do
		local repairSquadID = repairSquadsCopy[i]
		local repairSquadInfo = AAI_C_SquadInfo[repairSquadID]
		if Squad_IsValid(repairSquadID) and repairSquadInfo then
			repairSquadInfo:Unlock()
			Cmd_Stop(SG(Squad_FromID(repairSquadID)))
		end
	end
	self.repairSquads = {}
end

function AAI_C_SquadInfoClass:OnPriorityQueueUpdate()
	local healthPercent = Squad_GetHealthPercentage(self.squad, true)
	if healthPercent >= 1.0-eps and #self.repairSquads > 0 then
		self:DismissRepairSquads()
		self.isBeingHealed = false
	else
		self.isBeingHealed = self.healthDelta > eps or #self.repairSquads > 0
	end
	self.healthDelta = healthPercent - self.lastUpdateHealth
	self.lastUpdateHealth = healthPercent
end

function AAI_C_SquadInfoClass:Update()
	self:UpdateBPNameReferences()

	local squad = self.squad
	local retreated = Squad_IsRetreating(squad)
	local lockStatus = self.lockStatus
	if retreated then
		if self.task then
			self.task:OnSquadTaskedAway(self)
		end

		self.pullbackCounterInfo:Reset()
		self.pullbackLockTimer = 0.0
		self.assaultLockTimer = 0.0
		self.helpTime = 0.0

		if self.activeAction then
			self.activeAction:Deactivate(self, false)
			self.activeAction = nil
		end

		self:Unlock()

		self.engineerControlLockTimer = 0.0
		if self.ecSetting then
			self:Lock(aai_lockStatus.EngineerControl)
		end
	else
		if self.task and (Squad_IsUnderAttack(squad, 6.0) or Squad_IsAttacking(squad, 6.0)) then
			self.task:OnSquadTaskedAway(self)
			QueuedMovement_ClearForSquadID(self.squadID)
		end

		self.pullbackCounterInfo:TryDecrease()

		local time = World_GetGameTime()

		if self.pullbackLockTimer > eps then
			if time > self.pullbackLockTimer then
				self:Unlock()
				self.pullbackLockTimer = 0.0
			end
		elseif self.lockStatus == AAI_LockStatus.OOCCustomPositioning then
			if time > self.taskArrivalLockTime then
				self.taskArrivalLockTime = 0.0
				self:Unlock()
			end
		elseif self.assaultLockTimer > eps then
			if time > self.assaultLockTimer then
				self.assaultLockTimer = 0.0
				self:Unlock()
			end
		elseif self.helpTime > eps then
			if time > self.helpTime then
				self.helpTime = 0.0
				if lockStatus == aai_lockStatus.Help or lockStatus == aai_lockStatus.CUC then
					self:Unlock()
				end
			end
		end

		if self.activeAction then
			self.activeAction:Update(self)
		end
	end
end

---@param upgPBG UpgradePBG
---@return boolean
function AAI_C_SquadInfoClass:HasAppliedUpgrade(upgPBG)
	if Squad_HasUpgrade(self.squad, upgPBG) then return true end
	local bpName = BP_GetName(upgPBG)
	for i=1,#self.appliedUpgrades do
		if bpName == self.appliedUpgrades[i] then
			return true
		end
	end
	return false
end
