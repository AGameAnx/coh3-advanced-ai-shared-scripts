---@diagnostic disable: missing-fields

---@class AAI_C_CoverDataRefreshParams : class
---@field precision number?
---@field entitySearchRadiusMult number?
---@field heightMod number?
---@field distanceMult number?
---@field distanceExponent number?
---@field terrainMult number?
---@field enemyAvoidRadius number?
---@field enemyAvoidMod number?
---@field enemyAvoidMod_Vehicle number?
---@field friendlyAvoidRadius number?
---@field friendlyAvoidMod number?
---@field friendlyAvoidMod_Vehicle number?
---@field distanceFromOriginMult number?
---@field directionTowardsEnemyMult number?
---@field directionTowardsEnemyExponent number?
---@field directionSimilarityMult number?
---@field directionSimilarityExponent number?
---@field directionSimilarityMod number?
---@field directionSimilarityThreshold number?
---@field directionBreakThreshold number?
---@field directionBreakMod number?
---@field coverNoDirectionMult number?
---@field movementDirectionMod number?
---@field totalCoverMult number?
---@field maxCoverMult number?
---@field impass number?
---@field inLOS number?
---@field noLOS number?
---@field preferCQC_inLOS number?
---@field preferCQC_noLOS number?
AAI_CoverSearchParams = {
	__skipsave = true,

	precision = 1.25,
	baseValue = 0.25,
	entitySearchRadiusMult = 0.1,
	heightMod = 0.5,
	distanceMult = 0.85,
	distanceExponent = 1.0,
	terrainMult = 0.5,
	enemyAvoidRadius = 7.65,
	enemyAvoidMod = 0.02,
	enemyAvoidMod_Vehicle = 4.5,
	friendlyAvoidRadius = 5.8,
	friendlyAvoidMod = 0.25,
	friendlyAvoidMod_Vehicle = 0.0,
	preferCQC_enemyRadius = 20.0,
	preferCQC_enemyPreferenceMod = 1.1,
	distanceFromOriginMult = 0.25,
	directionTowardsEnemyMult = 0.85,
	directionTowardsEnemyExponent = 0.6,
	directionSimilarityMult = 0.5,
	directionSimilarityExponent = 1.0,
	directionSimilarityMod = 0.55,
	directionSimilarityThreshold = 0.9,
	directionBreakThreshold = -0.05,
	directionBreakMod = 0.5,
	coverNoDirectionMult = 0.15,
	movementDirectionMod = 0.45,
	totalCoverMult = 0.5,
	maxCoverMult = 1.0,
	impass = 0.15,
	inLOS = 1.0,
	losCountScaling = 0.7, -- How much los modifiers are affected by the total percentage of the nearby enemy units that have los on a cell
	noLOS = 0.5,
	preferCQC_inLOS= 1.0,
	preferCQC_noLOS = 1.0,
	mgarc = 85.0,
	mgarc_min = 55.0,
	mgarcMod = 0.2,
}

---@param t AAI_C_CoverDataRefreshParams
---@return AAI_C_CoverDataRefreshParams
function AAI_CoverSearchParams:new(t)
	t.__index = self
	setmetatable(t, t)
	return t
end

AAI_CoverSearchParams_assault = AAI_CoverSearchParams:new{
	baseValue = 2.0,
	precision = 3.0, -- Note: Lowering this value is performance-intensive due to pathfinding
	distanceMult = 0.15,
	totalCoverMult = 0.35,
	maxCoverMult = 0.35,
	distanceFromOriginMult = 0.0,
	inLOS = 0.5,
	inLOS_preferCloseRange = 0.1,
	noLOS = 1.0,
	noLOS_preferCloseRange = 1.0,
}
AAI_CoverSearchParams_noDistanceMod = AAI_CoverSearchParams:new{distanceMod=0.0}

AAI_CoverSearchParams_wire = AAI_CoverSearchParams:new{
	precision = 0.75,
	heightMod = 0.0,
	terrainMult = 0.0,
	enemyAvoidRadius = 0.0,
	enemyAvoidMod = 0.0,
	enemyAvoidMod_Vehicle = 0.0,
	friendlyAvoidRadius = 5.8,
	friendlyAvoidMod = 0.0,
	friendlyAvoidMod_Vehicle = 10.0,
	distanceMult = 0.4,
	distanceFromOriginMult = 0.0,
	directionTowardsEnemyExponent = 0.4,
	directionSimilarityMod = 0.95,
	directionBreakMod = 0.5,
	coverNoDirectionMult = 0.0,
	movementDirectionMod = 0.0,
	totalCoverMult = 0.15,
	maxCoverMult = 0.5,
}

AAI_CoverSearchParams_sandbag = AAI_CoverSearchParams:new{
	precision = 1.75,
	heightMod = 0.0,
	terrainMult = 1.5,
	enemyAvoidRadius = 0.0,
	enemyAvoidMod = 0.0,
	enemyAvoidMod_Vehicle = 0.0,
	friendlyAvoidRadius = 5.8,
	friendlyAvoidMod = 0.0,
	friendlyAvoidMod_Vehicle = 10.0,
	distanceMult = 0.4,
	distanceFromOriginMult = 0.0,
	directionTowardsEnemyMult = 0.65,
	directionTowardsEnemyExponent = 0.4,
	directionSimilarityMult = 0.0,
	directionSimilarityMod = 0.0,
	directionBreakMod = 0.0,
	coverNoDirectionMult = 0.35,
	movementDirectionMod = 0.0,
	totalCoverMult = 0.25,
	maxCoverMult = 1.0,
}
